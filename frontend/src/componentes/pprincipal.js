//PAGINA PRINCIPAL
import React from 'react';
import { Container, Button } from 'react-bootstrap';
import './estilo/pprincipal.css';
class Principal extends React.Component {

    render() {

        let elemento =

            <Container className="general">
                <br />
                <h1 className="titulo">BOSA</h1><br /><br />
                <h3>Significado: "Cercado del que guarda y defiende las mieses"</h3>
                <h4 className='t1'>Bosa fue un importante poblado muisca durante la época precolombina, era gobernado por el cacique Techovita a la llegada de los españoles.</h4>


                <div className='p2'>
                    <Button className="button" variant='outline-dark' href='/vidaenlalocalidad'>Vida en la localidad</Button>
                    <Button className="button" variant='outline-dark' href='/sitiosdeinteres'>Sitios de interés</Button>
                </div><br />
                <h5>Bosa es la localidad número siete del Distrito Capital de Bogotá, Capital de Colombia. Se encuentra ubicada al suroccidente de la ciudad.</h5><br></br>
            </Container >;

        return elemento;
    }
}

export default Principal;