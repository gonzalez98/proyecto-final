import React, { Component } from 'react';
import './estilo/vidaenlalocalidad.css';
//import L from 'leaflet';
import { Map, TileLayer } from 'react-leaflet';




class Mapa extends Component {

    state = {
        greenIcon: {
            lat: 35.787449,
            lng: -78.6438197,
        },
        redIcon: {
            lat: 35.774416,
            lng: -78.633271,
        },
        orangeIcon: {
            lat: 35.772790,
            lng: -78.652305,
        },
        zoom: 15
    }  

    render() {

        return (
            <Map className="map" center={[4.611160250152406, -74.18346405029297]} zoom={this.state.zoom}>
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
            </Map>
        );
    }
}

export default Mapa;

