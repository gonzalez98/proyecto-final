//FORMULARIO  PARA AGREGAR UN EVENTO
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Container, Button, Navbar, Nav } from 'react-bootstrap';
class CrearEvento extends React.Component {

    enviarForm(valores, acciones) {

        console.log(valores);
        let datos = {
            id: valores.id,
            valor: valores.valor
        }
        fetch(
            'http://localhost:3000/vidaenlalocalidad/eventos',
            {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    '': 'Access-Control-Allow-Origin'
                },
                body: JSON.stringify(datos)
            }
        );
    };
    render() {
        let elemento = <Formik
            initialValues={
                {
                    id: '',
                    valor: ''
                }
            }
            onSubmit={this.enviarForm}
            validationSchema={Yup.object().shape(
                {
                    id: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    valor: Yup.string().required('Campo es obligatorio')

                }
            )}>

            <Container className="p-3">
                <Navbar bg="light" variant="light" className='nav'>
                    <Navbar.Brand href="/">BOSA</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href=""></Nav.Link>
                        <Nav.Link href="/vidaenlalocalidad">Vida en la localidad</Nav.Link>
                        <Nav.Link href="/sitiosdeinteres">Sitios de interés</Nav.Link>
                    </Nav>
                </Navbar>
                <h1 className='titulo1'>Crea un nuevo evento</h1>
                <Form className='p1'>
                    <div className="form-group">
                        <label htmlFor='id'>ID</label>
                        <Field name="id" type="text" className="form-control" />
                        <ErrorMessage name="id" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Nombre del evento</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Barrio</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Dirección</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Fecha y Hora</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Descripción del evento</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <label htmlFor='valor'>Restricciones</label>
                        <Field name="valor" type="text" className="form-control" />
                        <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                    </div>
                    <div className="form-group">
                        <Button type="submit" className="btn btn-primary m-4">Aceptar</Button>
                        <Button type="reset" href='/vidaenlalocalidad' className="btn btn-secondary">Cancelar</Button>
                    </div>
                </Form>
            </Container>
        </Formik>;

        return elemento;

    };



}

export default CrearEvento;
