//PAGINA VIDA EN LA LOCALIDAD
import React from 'react';
import { Container, Button, Navbar, Nav } from 'react-bootstrap';
//import './estilo/vidaenlalocalidad.css';
import Cesium from './mapacesium';

window.CESIUM_BASE_URL = 'https://http://localhost:3000/sitiosdeinteres';

class Sitios extends React.Component {

    render() {

        
        let elemento =

            <Container >
                <Navbar bg="light" variant="light" className='nav'>
                    <Navbar.Brand href="/">BOSA</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href=""></Nav.Link>
                        <Nav.Link href="/vidaenlalocalidad">Vida en la localidad</Nav.Link>
                    </Nav>
                </Navbar>
                <br />
                <div className='p1'>
                    <div>
                        <h1 className='titulo1'>SITIOS DE INTERES</h1>
                    </div><br/>
                    <div><h2>Sitios</h2></div>
                    <Cesium className='visor' />
                    <Button className="button1" variant='outline-dark' href='/sitiosdeinteres/sitios'>Compartenos tu sitio</Button>
                </div>

            </Container>;
        return elemento;
    }

} 

export default Sitios;