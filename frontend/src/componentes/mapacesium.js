import React from "react";
import { Viewer, Entity, PointGraphics } from "resium";
import { Cartesian3 } from "cesium";
const position = Cartesian3.fromDegrees(-74.0707383, 40.7117244, 100);
class Cesium extends React.PureComponent {

  render() {
    return (
      <Viewer>
        <Entity position={position} name="Tokyo" description="Hello, world.">
          <PointGraphics pixelSize={10} />
        </Entity>
      </Viewer>
    );
  }

}

export default Cesium;