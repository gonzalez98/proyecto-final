//PAGINA VIDA EN LA LOCALIDAD

import React from 'react';
import { Container, Button, Navbar, Nav } from 'react-bootstrap';
import './estilo/vidaenlalocalidad.css';
import Mapa from './mapaleaflet';
 


class Localidad extends React.Component {

    
    render() {

        
        let elemento =

            <Container >
                <Navbar bg="light" variant="light" className='nav'>
                    <Navbar.Brand href="/">BOSA</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href=""></Nav.Link>
                        <Nav.Link href="/sitiosdeinteres">Sitios de interés</Nav.Link>
                    </Nav>
                </Navbar>
                <br />
                <div className='p1'>
                    <div>
                        <h1 className='titulo1'>VIDA EN LA LOCALIDAD</h1>
                    </div><br/>
                    <div><h2>Eventos</h2></div>
                    <Mapa/>
                    <Button className="button1" variant='outline-dark' href='/vidaenlalocalidad/eventos'>Compartenos tu evento</Button>
                </div>

            </Container>;
        return elemento;
    }

}

export default Localidad;