import React from 'react';
//import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import './app.css';

import Principal from './componentes/pprincipal';
import Localidad from './componentes/vidaenlalocalidad';
import Sitios from './componentes/sitiosdeinteres';
import CrearSitio from './componentes/crearsitio';
import CrearEvento from './componentes/crearevento';

function App(){

    return (
        <Router>
            <Route exact path='/' component={Principal}/>
            <Route exact path='/vidaenlalocalidad' component={Localidad}/>
            <Route exact path='/sitiosdeinteres' component={Sitios}/>
            <Route exact path='/vidaenlalocalidad/eventos' component={CrearEvento}/>
            <Route exact path='/sitiosdeinteres/sitios' component={CrearSitio}/>
        </Router>
    );
}

export default App; 