class Contestador {
    responder(peticion, respuesta) {
      respuesta.send("Hola Mundo de Backend!!!");
    }
  
    responderSaludo(peticion, respuesta) {
      let a = 13;
      let b = 8;
      respuesta.send("respuesta" + (a + b));
    }
  
    insertar(peticion, respuesta) {
      const mysql = require("mysql");
  
      const configuracion = {
        host: "localhost",
        user: "usuario1",
        password: "123456789usuario",
        database:"databasepf"
      };
      const conexion = mysql.createConnection(configuracion);
      conexion.connect((err)=>{if (err) {
          console.log("Error al conectarse a la base de datos");
          return;
        }
        console.log("Conexión establecida");});
  
      //Estos datos TIENEN QUE SER "LIMPIADOS". Verificar que cumplan con los parámetros.
      const registro = { 
        id: '', 
        valor: '' 
      };
  
      conexion.query('INSERT INTO tabla SET ?', registro, (err, res) => {
      if(err) throw err;
        console.log('Último registro:', res.insertId);
      });
  
      conexion.end();
  
      respuesta.send("Conexión Cerrada");
    }
  