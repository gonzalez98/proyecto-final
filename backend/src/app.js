
let express = require('express');
let cors = require('cors');
let app = express();


//Settings 
app.set('port', 4000);

//Middlewares: Funciones a ejecutar antes de llegar a las URL
 //cors Permite coder conectar servidores entre ellos
 app.use(cors());
 app.use(express.json());

//Routes

app.use('/', require('./routes/pinicio'));
app.use('/vidaenlalocalidad', require('./routes/vidaenlalocalidad'));
app.use('/sitiosdeinteres', require('./routes/sitiosdeinteres'));
app.use('/vidaenlalocalidad/eventos', require('./routes/eventos'));
app.use('/sitiosdeinteres/sitios', require('./routes/sitios'));





module.exports = app;


